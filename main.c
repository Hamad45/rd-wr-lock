#include<stdio.h>
#define __USE_UNIX98
#include<pthread.h>

pthread_rwlock_t rwlock;
void* lcd(void* data)
{
	pthread_rwlock_rdlock(&rwlock);
	printf("lcd critical section \n");
	sleep(5);
	pthread_rwlock_unlock(&rwlock);
}

void* serial(void* data)
{
	pthread_rwlock_rdlock(&rwlock);
	printf("serial critical section \n");
	sleep(7);
	pthread_rwlock_unlock(&rwlock);
}

void* update(void* data)
{
	pthread_rwlock_wrlock(&rwlock);
	printf("update critical section \n");
	sleep(5);
	pthread_rwlock_unlock(&rwlock);	
}

int main()
{
//	while(1)
//	{
		pthread_t idlcd,idserial,idupdate;
	
		pthread_rwlock_init(&rwlock,NULL);
		pthread_create(&idlcd,NULL,lcd,NULL);
		pthread_create(&idserial,NULL,serial,NULL);
		pthread_create(&idupdate,NULL,update,NULL);
	
		pthread_join(&idlcd,NULL);
		pthread_join(&idserial,NULL);
		pthread_join(&idupdate,NULL);
		pthread_rwlock_destroy(&rwlock);
//	}
	return 0;
}

